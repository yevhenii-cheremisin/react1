import React from "react";
import "./Message.css";

class Message extends React.Component {
  constructor() {
    super();
    this.liked = this.liked.bind(this);
  }
  state = {
    isLiked: false,
  };

  liked() {
    if (this.state.isLiked === false) {
      this.setState({
        isLiked: true,
      });
    } else {
      this.setState({
        isLiked: false,
      });
    }
  }

  renderTime(createdAt) {
    const date = new Date(createdAt);
    return date.getHours() + ":" + date.getMinutes();
  }

  render() {
    let date = this.renderTime(this.props.data.createdAt);

    return (
      <div className="message">
        <img
          className="message-user-avatar"
          src={this.props.data.avatar}
          alt="avatar"
        />
        <div className="message-body">
          <div className="message-body_text">
            <div className="message-user-name">{this.props.data.user}</div>
            <span className="message-text">{this.props.data.text}</span>
          </div>
          <span className="message-time">{date}</span>
        </div>
        <button
          className={this.state.isLiked ? "message-liked" : "message-like"}
          onClick={this.liked}
        ></button>
      </div>
    );
  }
}

export default Message;
