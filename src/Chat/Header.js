import React from "react";
import "./Header.css";

class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <span className="header-title">Ozark Family</span>
        <span>|</span>
        <span className="header-users-count-title">Users in chat:</span>
        <span className="header-users-count">{this.props.userInChat}</span>
        <span>|</span>
        <span className="header-messages-count-title">Messages:</span>
        <span className="header-messages-count">{this.props.messages}</span>
        <span>|</span>
        <span className="header-last-message-date-title">Last Message:</span>
        <span className="header-last-message-date">
          {this.props.lastMessage}
        </span>
      </div>
    );
  }
}

export default Header;
