import React from "react";
import "./OwnMessage.css";

class OwnMessage extends React.Component {
  constructor() {
    super();
    this.editMes = this.editMes.bind(this);
    this.deleteMes = this.deleteMes.bind(this);
  }
  renderTime(createdAt) {
    const date = new Date(createdAt);
    return date.getHours() + ":" + date.getMinutes();
  }

  editMes() {
    let newText;
    newText = "edited"; //prompt("Enter new text");
    while (newText === undefined || newText === "") {
      newText = prompt("Enter new text");
    }
    const id = this.props.data.id;
    console.log("WORKING!");
    const dateEdit = new Date();
    this.props.edit(newText, id, dateEdit);
  }

  deleteMes() {
    const id = this.props.data.id;
    this.props.delete(id);
  }

  render() {
    let date = this.renderTime(this.props.data.createdAt);

    return (
      <div className="own-message">
        <button className="message-edit" onClick={this.editMes}>
          🖊️
        </button>
        <button className="message-delete" onClick={this.deleteMes}>
          🗑️
        </button>
        <div className="message-body">
          <div className="message-body_text-own">
            <span className="message-text">{this.props.data.text}</span>
          </div>
          <span className="message-time">{date}</span>
        </div>
      </div>
    );
  }
}

export default OwnMessage;
