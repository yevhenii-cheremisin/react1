import React from "react";
import MessageList from "./MessageList";
import Preloader from "./Preloader";
import MessageInput from "./MessageInput";
import Header from "./Header";
import { v4 as uuidv4 } from "uuid";

class Chat extends React.Component {
  constructor() {
    super();
    this.addMessage = this.addMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
  }
  state = {
    isLoading: true,
    messages: null,
    messageNumber: 0,
    usersInChat: 0,
    lastMessageSent: null,
  };

  renderTime(createdAt) {
    const date = new Date(createdAt);
    let day = date.getDay();

    const days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    day = days[day];

    return (
      date.getDate() +
      "." +
      (date.getMonth() + 1) +
      "." +
      date.getFullYear() +
      " " +
      date.getHours() +
      ":" +
      date.getMinutes()
    );
  }

  usersCount(array) {
    let isThere = [];
    let numOfUsers = 0;
    array.forEach((val) => {
      if (!isThere.includes(val.user)) {
        isThere.push(val.user);
        numOfUsers++;
      }
    });
    return numOfUsers;
  }

  async componentDidMount() {
    const url = this.props.url;
    const res = await fetch(url);
    const messagesData = await res.json();

    const usersInChatCount = this.usersCount(messagesData);

    const TodayDate = this.renderTime(
      messagesData[messagesData.length - 1].createdAt
    );

    if (messagesData.length !== 0) {
      this.setState({
        isLoading: false,
        messages: messagesData,
        messageNumber: messagesData.length,
        usersInChat: usersInChatCount,
        lastMessageSent: TodayDate,
      });
    }
    console.log(this.state.usersInChat);
  }

  addMessage(myText, clear) {
    const OldMessage = [...this.state.messages];
    let OldMessageCount = this.state.messageNumber;
    OldMessageCount++;
    const currentDate = new Date();
    const newMessage = {
      user: "Admin",
      createdAt: currentDate,
      id: uuidv4(),
      fromSender: true,
      text: myText,
    };

    OldMessage.push(newMessage);
    this.setState({
      messages: OldMessage,
      lastMessageSent: this.renderTime(currentDate),
      messageNumber: OldMessageCount,
    });
    clear();
  }

  editMessage(newText, id, date) {
    console.log("Im here!");
    const OldMessage = [...this.state.messages];
    let indexForEdited = OldMessage.findIndex((message) => {
      return message.id === id;
    });
    OldMessage[indexForEdited].text = newText;
    OldMessage[indexForEdited].createdAt = date;
    this.setState({
      messages: OldMessage,
    });
  }

  deleteMessage(id) {
    let OldMessage = [...this.state.messages];
    let OldMessageCount = this.state.messageNumber;
    OldMessageCount--;
    OldMessage = OldMessage.filter((val) => {
      if (val.id != id) {
        return val;
      }
    });
    this.setState({
      messages: OldMessage,
      messageNumber: OldMessageCount,
    });
  }

  render() {
    if (!this.state.isLoading) {
      return (
        <div className="chat">
          <Header
            messages={this.state.messageNumber}
            userInChat={this.state.usersInChat}
            lastMessage={this.state.lastMessageSent}
          />
          <MessageList
            messages={this.state.messages}
            edit={this.editMessage}
            delete={this.deleteMessage}
          />
          <MessageInput renderMessage={this.addMessage} />
        </div>
      );
    } else {
      return <Preloader />;
    }
  }
}

export default Chat;
