import React from "react";
import Message from "./Message";
import "./MessageList.css";
import OwnMessage from "./OwnMessage";

class MessageList extends React.Component {
  validDate(dateParsed) {
    return (
      dateParsed.getFullYear() + dateParsed.getDate() + dateParsed.getMonth()
    );
  }

  renderTime(createdAt) {
    const date = new Date(createdAt);
    const currentDate = new Date();
    let day = date.getDay();

    const days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    day = days[day];

    const month = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    const indexForMonth = date.getMonth();
    const monthMessage = month[indexForMonth];

    const validDate = date.getDate() + date.getFullYear() + date.getMonth();
    const validDateYesterday =
      date.getDate() + 1 + date.getFullYear() + date.getMonth();
    const validCurrentDay =
      currentDate.getDate() +
      currentDate.getFullYear() +
      currentDate.getMonth();

    if (validDate === validCurrentDay) {
      return "Today";
    }

    if (validDateYesterday === validCurrentDay) {
      return "Yesterday";
    }

    return day + ", " + date.getDate() + " " + monthMessage;
  }

  render() {
    let currentDateChecker = 0;

    return (
      <div className="message-list">
        {this.props.messages.map((value, index) => {
          let dateParsed = new Date(value.createdAt);
          let dateToCheck = this.validDate(dateParsed);
          let DateToSHow = this.renderTime(dateParsed);
          if (currentDateChecker != dateToCheck) {
            currentDateChecker = dateToCheck;
            return (
              <div key={index + 1.2}>
                <div className="messages-divider">{DateToSHow}</div>
                {value.fromSender ? (
                  <OwnMessage
                    data={value}
                    key={index}
                    edit={this.props.edit}
                    delete={this.props.delete}
                  />
                ) : (
                  <Message data={value} key={index} />
                )}
              </div>
            );
          }
          return value.fromSender ? (
            <OwnMessage
              data={value}
              key={index}
              edit={this.props.edit}
              delete={this.props.delete}
            />
          ) : (
            <Message data={value} key={index} />
          );
        })}
      </div>
    );
  }
}

export default MessageList;
