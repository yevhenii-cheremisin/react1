import React from "react";
import "./MessageInput.css";
import Message from "./Message";

class MessageInput extends React.Component {
  constructor() {
    super();
    this.state = { message: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.clear = this.clear.bind(this);
  }
  state = {
    message: null,
  };

  clear() {
    this.setState({
      message: "",
    });
  }

  handleClick() {
    const clearFunc = this.clear;
    if (!(this.state.message === undefined || this.state.message === "")) {
      this.props.renderMessage(this.state.message, clearFunc);
    }
  }

  handleChange(event) {
    this.setState({ message: event.target.value });
  }

  render() {
    return (
      <div className="message-input">
        <input
          className="message-input-text"
          type="text"
          value={this.state.message}
          onChange={this.handleChange}
        />
        <button className="message-input-button" onClick={this.handleClick}>
          Send
        </button>
      </div>
    );
  }
}

export default MessageInput;
