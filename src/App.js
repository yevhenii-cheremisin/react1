import "./App.css";
import Chat from "./Chat/Chat";

function App() {
  return (
    <>
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </>
  );
}

export default App;
